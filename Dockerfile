FROM ruby:2.7.1

RUN apt-get update -qq && apt-get install -y nodejs 
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs

#create a dir
RUN mkdir /docker-rails-template

# Change the dir
WORKDIR /docker-rails-template

# In your Dockerfile, change the PATH and GEM_HOME so that Bundler will install all gems to the same location, and running commands will use the RubyGems binstubs instead of Bundler’s application-locked binstubs:
ENV GEM_HOME="/usr/local/bundle"
ENV PATH $GEM_HOME/bin:$GEM_HOME/gems/bin:$PATH

COPY Gemfile /docker-rails-template/Gemfile
COPY Gemfile.lock /docker-rails-template/Gemfile.lock
RUN bundle install
COPY . /docker-rails-template


# Add a script to be executed every time the container starts.

#expose the port
EXPOSE 3001

# Start the main process.
CMD ["rails", "server", "-b", "0.0.0.0"]
